#!/usr/bin/env python
import util

client = util.getConnection()
book,sheet,sheet1 = util.openSheet()

dict_list = []

for row_index in range(1, sheet1.nrows):
    col_data = util.project_json(row_index,range(sheet1.ncols),sheet1,book)
    dict_list.append(col_data)
print(dict_list)

util.insert_project(client,dict_list)