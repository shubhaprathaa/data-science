#!/usr/bin/env python
import pymongo
import pprint
import pandas as pd
import json 
import xlrd,datetime
from xlrd import open_workbook
from pymongo import MongoClient
from xlrd.xldate import xldate_as_tuple


def getConnection():
       client = MongoClient('mongodb://localhost:27017/')
       print (client)
       return client

def getExcel():
       data = pd.read_excel("dataentry.xlsx")
       return data

def insertData(client):
       db = client.organisationdb
       collection = db.test_collection
       employee = {"employee_id":"771","firstname":"sarath","lastname":"kumar","department":"Development","dob":"10.04.1988","gender":"male","qualification":"B.E","nationality":"indian","phone_no":"9123045664","email_id":"sarath@gmail.com","street":"hsr layout","city":"chetpet","district":"chennai","state":"tamilnadu","pincode":"610001","bloodgroup":"o+","doj":"12.5.2015","designation":"manager","salary":"60,000"}
       employees = db.employees
       employee_id =employees.insert_one(employee).inserted_id
       if   db.employees.find({'UserIDS': { "$in": employee_id}}):
            
             print ("inserted"
)   
             print (employee_id)  
       else:
             
             print ("not inserted")

       for employee in employees.find():
           
            pprint.pprint(employee)

			
def printExcel(data):
      print(data)

	  
def openSheet():
    book = xlrd.open_workbook('Dataentry.xlsx')
    sheet = book.sheet_by_index(0)
    sheet1 = book.sheet_by_index(1)
    return book,sheet,sheet1	 

def employee_json(row_index,col_range,sheet,book):
    emp_id_value = int(sheet.cell(row_index,0).value)
    fn_value = sheet.cell(row_index,1).value
    ln_value = sheet.cell(row_index,2).value
    d_ment = sheet.cell(row_index,3).value
    d_o_b = sheet.cell(row_index,4).value
    gender = sheet.cell(row_index,5).value
    qualif = sheet.cell(row_index,6).value
    nation = sheet.cell(row_index,7).value
    ph_no = int(sheet.cell(row_index,8).value)
    em_id = sheet.cell(row_index,9).value
    street = sheet.cell(row_index,10).value
    city = sheet.cell(row_index,11).value
    dist = sheet.cell(row_index,12).value
    state = sheet.cell(row_index,13).value
    pin_code = int(sheet.cell(row_index,14).value)
    b_group = sheet.cell(row_index,15).value
    d_o_j = sheet.cell(row_index,16).value
    desig = sheet.cell(row_index,17).value
    salary = int(sheet.cell(row_index,18).value)
    bank_name = sheet.cell(row_index,19).value
    ifsc_code = sheet.cell(row_index,20).value
    a_c_no = int(sheet.cell(row_index,21).value)
    employee ={"employee_id":emp_id_value,
	           "firstname":fn_value,
			   "lastname":ln_value,
			   "department":d_ment,
			   "dob":datetime.datetime(*xlrd.xldate_as_tuple(d_o_b,book.datemode)),
			   "gender":gender,
			   "qualification":qualif,
			   "nationality":nation,
			   "phone_no":ph_no,
			   "email_id":em_id,
			   "street":street,
			   "city":city,
			   "district":dist,
			   "state":state,
			   "pincode":pin_code,
			   "bloodgroup":b_group,
			   "work_details":{
			     "doj":datetime.datetime(*xlrd.xldate_as_tuple(d_o_j,book.datemode)),
			     "designation":desig,
			     "salary":salary},
			   "bank_details":{
			     "bankname":bank_name,
			     "ifsc_code":ifsc_code,
			     "ac_no":a_c_no},
               }
    return employee


	
def insert_employee(client,dict_list):
    db = client.organisationdb
    db.employee.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.employee.count())
	
def client_json(row_index,col_range,sheet1,book):
    cli_id = int(sheet1.cell(row_index,0).value)
    cli_name = sheet1.cell(row_index,1).value
    cp_fname = sheet1.cell(row_index,2).value
    cp_lname = sheet1.cell(row_index,3).value
    email_id = sheet1.cell(row_index,4).value
    ph_no = int(sheet1.cell(row_index,5).value)
    req = sheet1.cell(row_index,6).value
    pro_id = sheet1.cell(row_index,8).value
    title = sheet1.cell(row_index,7).value
    client_details = {"client_id":cli_id,
              "client_name":cli_name,
              "contact_person":{
			     "firstname":cp_fname,
			     "lastname":cp_lname,
			     "email_id":email_id,
			     "phone_no":ph_no,
		       },
               "requirements":req,
               "project_id":pro_id,
               "title":title
              }
    return client_details
			   
def insert_client(client,dict_list):
    db = client.organisationdb
    db.client_details.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.client_details.count())

def project_json(row_index,col_range,sheet1,book):
    pro_id = sheet1.cell(row_index,8).value
    title = sheet1.cell(row_index,7).value
    cli_id = int(sheet1.cell(row_index,0).value)
    st_date = sheet1.cell(row_index,9).value
    rel_date = sheet1.cell(row_index,10).value
    status = sheet1.cell(row_index,11).value
    employ_1 = int(sheet1.cell(row_index,12).value)
    employ_2 = int(sheet1.cell(row_index,13).value)
    project = {"project_id":pro_id,
	           "title":title,
	           "client_id":cli_id,
               "start_date":datetime.datetime(*xlrd.xldate_as_tuple(st_date,book.datemode)),
               "release_date":datetime.datetime(*xlrd.xldate_as_tuple(rel_date,book.datemode)),
               "status":status,
               "team":[employ_1,employ_2]
            }
    return project

def insert_project(client,dict_list):
    db = client.organisationdb
    db.project.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.project.count())

			   
			   
   
    
      



